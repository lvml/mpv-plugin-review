-- This script allows to delete video clips from within mpv, useful when
-- reviewing lots of (mostly useless) unedited recordings.

utils = require 'mp.utils'

review_fname = "/tmp/does/not/exist"

function review_on_loaded()

        review_fname = mp.get_property_native("path")

        mp.msg.log("v", "reviewing file '" .. review_fname .. "'")
        mp.osd_message("reviewing '" .. review_fname .. "',\nPress Y to delete", 999)

        mp.set_property("loop-file", "inf")
end

mp.register_event("file-loaded", review_on_loaded)

function review_delete()
        local p = {}
        p["cancellable"] = "no"
        p["args"] = {}
        p["args"][1] = "rm"
        p["args"][2] = "-v"
        p["args"][3] = "--"
        p["args"][3] = review_fname
        local res = utils.subprocess(p)

        if (res["error"] ~= nil) then
                local emsg = "failed to execute 'rm -v -- " .. review_fname .. "\nError message: " .. res["error"]
                mp.msg.log("info", emsg)
                mp.osd_message(emsg, 999)
        end

        local emsg = "removed '" .. review_fname .. "'"
        mp.msg.log("info", emsg) 
        mp.osd_message(emsg, 999)
end


mp.add_key_binding("shift+y", "review_delete", review_delete, { repeatable = false; complex = false })

