Reviewing / removing files from within mpv
==========================================

The review.lua script allows you to remove files viewed
quickly from within mpv, with a single press of "Y"
(shift-y).

(written by Lutz Vieweg)

Rationale / Use Case:
=====================

When you have shot lots of movie clips with a camera,
chances are that you do not want to retain most of them,
as a ratio of 1 "good" shot to 10 "bad" shots is not
uncommon.

To save storage memory and effort when removing no longer needed
files, this simple plugin is for you.

Prerequisites / Installation
============================

In order to use review.lua, you'll just need "mpv", and your
operating system needs to understand the "rm -v ..." command -
as Linux does, and likely most other Unix derivatives.

Usage:
======

Invoke mpv with a (potentially long) list of video files,
this is most easy done from a shell, usind wildcard parameters
like *.mp4 to select what files to review.

Before the list of files, prepend the following mpv parameters:

--load-scripts=no --script=review.lua --pause=yes -- 

... prepended optionally for better looks with even more options:

--script-opts=osc-layout=bottombar,osc-hidetimeout=120000 \
--osd-font-size=20 --osd-color=0.5/0.5/0.5 

Standard key assignments useful:

* skip to the next file pressing RETURN or ">"
* skip to the previous file pressing "<"

If you want to remove the file currently viewed, press "Y" (shift-y).

To make this plugin safe to operate, per-file looping is enabled,
such that you will not accidently delete the wrong file because
mpv just started to play the next file in the moment you pressed "Y".
This of course means that you will always need to skip to the next
file manually.

DISCLAIMER
==========

This software is provided as-is, without any warranties.
